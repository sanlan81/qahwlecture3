package lectuter3;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class GeneralActions {
    private WebDriver driver;
    private WebDriverWait wait;

    public GeneralActions(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, 30);
    }

    public void login(String login, String password) {

        WebElement emailField = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("email"))));
        emailField.sendKeys(login);

        WebElement passwordField = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("passwd"))));
        passwordField.sendKeys(password);

        WebElement submitLogin = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.name("submitLogin"))));
        submitLogin.click();
    }

    public void createCategory(String categoryName) {
        WebElement newCategoryName = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id("name_1"))));
        newCategoryName.sendKeys(categoryName);
    }

    //Only for Id
    // TODO manager for clicking
    public void clickButton(String click) {
        WebElement clickButton = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(click))));
        clickButton.click();
    }

    public void categoryItem(String click) throws InterruptedException {
        Actions builder = new Actions(driver);
        WebElement mainMenu = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.id(click))));
        builder.moveToElement(mainMenu).build().perform();
        WebElement subMenu = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//a[contains(text(),'категории')]/..")))); //Find the submenu
        builder.moveToElement(subMenu).click().build().perform();
    }

    public void findingElement(String click) {
        WebElement clickButton = wait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.cssSelector(click))));
        clickButton.sendKeys("Shirts");
    }
}
