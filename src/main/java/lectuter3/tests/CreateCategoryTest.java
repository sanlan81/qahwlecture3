package lectuter3.tests;


import lectuter3.BaseScript;
import lectuter3.GeneralActions;

import lectuter3.utils.WebDriverLogger;
import org.apache.log4j.BasicConfigurator;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.concurrent.TimeUnit;

import static lectuter3.utils.Properties.getBaseAdminUrl;


public class CreateCategoryTest extends BaseScript {
    public static void main(String[] args) throws InterruptedException {

        EventFiringWebDriver driver =
                new EventFiringWebDriver(getDriver());
        driver.register(new WebDriverLogger());
        BasicConfigurator.configure();

        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.get(getBaseAdminUrl());

        driver.manage().window().maximize();

        // login
        GeneralActions generalActions = new GeneralActions(driver);
        generalActions.login("webinar.test@gmail.com", "Xcg7299bnSmMuRLp9ITw");

        // category Item
        generalActions.categoryItem("subtab-AdminCatalog");

        // create category
        generalActions.clickButton("page-header-desc-category-new_category");
        generalActions.createCategory("Shirts");
        generalActions.clickButton("category_form_submit_btn");

        // check that new category appears in Categories table
        generalActions.findingElement("input[name=categoryFilter_name]");
        generalActions.clickButton("submitFilterButtoncategory");

        // finish script
        driver.quit();

    }
}
